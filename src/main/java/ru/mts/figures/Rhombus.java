package main.java.ru.mts.figures;


import main.java.ru.mts.base.WithInscribedCircle;
import java.util.Arrays;

public class Rhombus extends Polygon implements WithInscribedCircle {

    /**
     * @param vertices An array with coordinates of vertices.
     *                 NOTE: Surrounding elements of array define consecutive vertices.
     *                 The first and the last vertex are consecutive by default.
     */
    public Rhombus(double[][] vertices) {
        super(4, vertices);
    }

    /**
     * @return Length of a side
     */
    public double getSideLength() {
        return getDistanceBetweenVertices(1, 2);
    }

    /**
     * @return Sorted array with lengths of diagonals (ascending order)
     */
    public double[] getDiagonalSizes() {
        double[] diagonals = new double[] {
            getDistanceBetweenVertices(1, 3),
            getDistanceBetweenVertices(2, 4)
        };

        Arrays.sort(diagonals);
        return diagonals;
    }

    /**
     * @return Radius of an inscribed circle of rhombus
     */
    @Override
    public double calculateInscribedCircleRadius() {
        double[] diagonals = getDiagonalSizes();
        return diagonals[0] * diagonals[1] / (4 * getSideLength());
    }
}
