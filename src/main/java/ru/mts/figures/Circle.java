package main.java.ru.mts.figures;

import main.java.ru.mts.base.GeomFigure;

/**
 * Basi Circle class
 */
public class Circle extends GeomFigure {

    private double radius;
    private double center_x;
    private double center_y;
    public String color;

    /**
     * @param radius Radius of the circle
     * @param center_x Center coordinate of the circle on x-axis
     * @param center_y Center coordinate of the circle on y-axis
     * @param color Color
     */
    public Circle(double radius, double center_x, double center_y, String color) {
        this.radius = radius;
        this.center_x = center_x;
        this.center_y = center_y;
        this.color = color;
    }

    /**
     * Changes a color of the circle.
     * @param new_color New color of the circle
     */
    public void colorCircle(String new_color) {
        color = new_color;
    }

    /**
     * Changes center coordinates of the circle.
     * @param new_center_x New center coordinate of the circle on x-axis
     * @param new_center_y New center coordinate of the circle on y-axis
     */
    public void moveCenter(double new_center_x, double new_center_y) {
        center_x = new_center_x;
        center_y = new_center_y;
    }

    /**
     * @return An array with center coordinates {x-axis, y-axis}
     */
    public double[] getCenter() {
        return new double[] {center_x, center_y};
    }

    /**
     * @return Perimeter of a circle
     */
    @Override
    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    /**
     * @return Area of a circle
     */
    @Override
    public double getArea() {
        return Math.PI * Math.pow(radius, 2);
    }
}
