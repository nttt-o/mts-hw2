package main.java.ru.mts.figures;

import main.java.ru.mts.base.GeomFigure;

import java.util.Arrays;
import java.util.List;

public class Triangle extends GeomFigure {

    private double[][] vertices;

    /**
     * @param vertices An array with coordinates of vertices.
     *                 NOTE: Surrounding elements of array define consecutive vertices.
     *                 The first and the last vertex are consecutive by default.
     */
    public Triangle(double[][] vertices) {
        this.vertices = vertices;
    }

    /**
     * @param vertexNum1 Number of the first vertex (>= 1)
     * @param vertexNum2 Number of the second vertex (>= 1)
     * @return The distance between two given vertices.
     */
    private double getDistanceBetweenVertices(int vertexNum1, int vertexNum2) {
        double[] vert_1 = vertices[vertexNum1 - 1];
        double[] vert_2 = vertices[vertexNum2 - 1];

        return Math.sqrt(Math.pow(vert_2[0] - vert_1[0], 2) + Math.pow(vert_2[1] - vert_1[1], 2));
    }


    /**
     * Calculates Length of a side defined by two vertex numbers.
     * @param vertexNum1 Number of the first vertex (>= 1)
     * @param vertexNum2 Number of the second vertex (>= 1)
     * @return Length of a side
     */
    public double getSideLength(int vertexNum1, int vertexNum2) {

        double sideSize = 0;
        List<Integer> vertexNums = Arrays.asList(1, 2, 3);

        int vertNumMin = Math.min(vertexNum1, vertexNum2);
        int vertNumMax = Math.max(vertexNum1, vertexNum2);

        if (vertexNums.contains(vertNumMin) && vertexNums.contains(vertNumMax) && vertNumMin != vertNumMax) {
            sideSize = getDistanceBetweenVertices(vertNumMin, vertNumMax);
        }
        return sideSize;
    }

    /**
     * @return Perimeter of a triangle
     */
    @Override
    public double getPerimeter() {
        return getDistanceBetweenVertices(1, 2) + getDistanceBetweenVertices(2, 3) + getDistanceBetweenVertices(1, 3);
    }

    /**
     * @return Area of a triangle
     */
    @Override
    public double getArea() {

        double halfPerimeter = getPerimeter() / 2;
        double side_1 = getDistanceBetweenVertices(1, 2);
        double side_2 = getDistanceBetweenVertices(2, 3);
        double side_3 = getDistanceBetweenVertices(1, 3);

        return Math.sqrt(halfPerimeter * (halfPerimeter - side_1) * (halfPerimeter - side_2) * (halfPerimeter - side_3));
    }

}
