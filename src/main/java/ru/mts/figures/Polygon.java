package main.java.ru.mts.figures;

import main.java.ru.mts.base.GeomFigure;
import main.java.ru.mts.base.WithAngles;

import java.util.ArrayList;
import java.util.List;

public class Polygon extends GeomFigure implements WithAngles {

    protected int vertexNum;
    protected double[][] vertices;

    /**
     * @param vertexNum A number of vertices in a polygon
     * @param vertices An array with coordinates of vertices.
     *                 NOTE: Surrounding elements of array define consecutive vertices.
     *                 The first and the last vertex are consecutive by default.
     */
    public Polygon(int vertexNum, double[][] vertices) {
        this.vertexNum = vertexNum;
        this.vertices = vertices;
    }

    /**
     * @param vertexNum1 Number of the first vertex (>= 1)
     * @param vertexNum2 Number of the second vertex (>= 1)
     * @return The distance between two given vertices.
     */
   protected double getDistanceBetweenVertices(int vertexNum1, int vertexNum2) {
        double[] vert_1 = vertices[vertexNum1 - 1];
        double[] vert_2 = vertices[vertexNum2 - 1];

        return Math.sqrt(Math.pow(vert_2[0] - vert_1[0], 2) + Math.pow(vert_2[1] - vert_1[1], 2));
    }

    /**
     * @return Perimeter of a polygon
     */
    @Override
    public double getPerimeter() {
        // get numbers of consecutive vertices
        List<int[]> combs = new ArrayList<>();
        for (int i = 1; i < vertexNum; i++) {
            combs.add(new int[] {i, i+1});
        }
        combs.add(new int[] {1, vertexNum});

        double perimeter = 0;
        for (int[] pair : combs) {
            perimeter += getDistanceBetweenVertices(pair[0], pair[1]);
        }

        return perimeter;
    }

    /**
     * @return Area of a polygon
     */
    @Override
    public double getArea() {
        double sum = 0;
        for (int i = 0; i < vertexNum; i++) {
            sum += vertices[i][0] * (vertices[(i + 1) % vertexNum][1] - vertices[(i + vertexNum - 1) % vertexNum][1]);
        }
        return Math.abs(sum / 2);
    }

    /**
     * Prints coordinates of the vertices with precision of 3 digits.
     */
    @Override
    public void showAllVertexCoordinates() {
        List<String> stringCoordinates = new ArrayList<>();

        for (double[] pair: vertices) {
            stringCoordinates.add(String.format("(%,.3f; %,.3f)", pair[0], pair[1]));
        }

        System.out.println(String.join(", ", stringCoordinates));
    }

}

