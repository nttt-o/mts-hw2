package main.java.ru.mts.base;

public abstract class GeomFigure {

    public abstract double getPerimeter();

    public abstract double getArea();

}
