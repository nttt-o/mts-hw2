package main.java.ru.mts.base;

public interface WithInscribedCircle {
    double calculateInscribedCircleRadius();

}
