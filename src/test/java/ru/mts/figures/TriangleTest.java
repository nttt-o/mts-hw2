package test.java.ru.mts.figures;

import main.java.ru.mts.figures.Triangle;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TriangleTest {

    double[] v1 = new double[] {0, 4};
    double[] v2 = new double[] {0, 0};
    double[] v3 = new double[] {3, 0};
    double[][] inputVertices = new double[][] {v1, v2, v3};
    Triangle triangle = new Triangle(inputVertices);

    @Test
    void testGetSideLength() {
        assertEquals(4, triangle.getSideLength(2, 1));
        assertEquals(3, triangle.getSideLength(2, 3));
        assertEquals(5, triangle.getSideLength(1, 3));
    }

    @Test
    void testGetPerimeter() {
        assertEquals(12, triangle.getPerimeter());
    }

    @Test
    void testGetArea() {
        assertEquals(6, triangle.getArea());
    }
}