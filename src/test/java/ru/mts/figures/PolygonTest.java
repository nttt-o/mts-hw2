package test.java.ru.mts.figures;

import main.java.ru.mts.figures.Polygon;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;



class PolygonTest {

    double[] v_1 = new double[] {1, 1};
    double[] v_2 = new double[] {3, 5};
    double[] v_3 = new double[] {8, 5};
    double[] v_4 = new double[] {7, 1};

    double[][] vertices = new double[][] {v_1, v_2, v_3, v_4};

    Polygon polygon = new Polygon(4, vertices);

    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }

    @Test
    void showAllVertexCoordinates() {
        String expected = "(1,000; 1,000), (3,000; 5,000), (8,000; 5,000), (7,000; 1,000)";
        polygon.showAllVertexCoordinates();
        assertEquals(expected, outputStreamCaptor.toString().trim());
    }

    @Test
    void getPerimeter() {
        assertEquals(19.6, Math.round(polygon.getPerimeter() * 100.0) / 100.0);
    }

    @Test
    void getArea() {
        assertEquals(22, polygon.getArea());
    }

}