package test.java.ru.mts.figures;

import main.java.ru.mts.figures.Circle;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CircleTest {

    Circle circle = new Circle(3, -1.5, 1.5, "green");

    @Test
    void colorCircle() {
        assertEquals("green", circle.color);
        circle.colorCircle("blue");
        assertEquals("blue", circle.color);
    }

    @Test
    void getPerimeter() {
        assertEquals(2 * Math.PI * 3, circle.getPerimeter());
    }

    @Test
    void getArea() {
        assertEquals(Math.PI * 3 * 3, circle.getArea());
    }

    @Test
    void moveCenter() {
        assertArrayEquals(new double[] {-1.5, 1.5}, circle.getCenter());
        circle.moveCenter(1.123, 4.456);
        assertArrayEquals(new double[] {1.123, 4.456}, circle.getCenter());
    }


}