package test.java.ru.mts.figures;

import main.java.ru.mts.figures.Rhombus;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class RhombusTest {

    double[] v1 = new double[] {0, 3};
    double[] v2 = new double[] {4, 6};
    double[] v3 = new double[] {8, 3};
    double[] v4 = new double[] {4, 0};
    double[][] inputVertices = new double[][] {v1, v2, v3, v4};

    Rhombus rhombus = new Rhombus(inputVertices);

    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }

    @Test
    void getSideLength() {
        assertEquals(5, rhombus.getSideLength());
    }

    @Test
    void getDiagonalSizes() {
        double[] diagonals = new double[] {6, 8};
        assertArrayEquals(diagonals, rhombus.getDiagonalSizes());
    }

    @Test
    void getPerimeter() {
        assertEquals(5 * 4, rhombus.getPerimeter());
    }

    @Test
    void getArea() {
        assertEquals(24, rhombus.getArea());
    }

    @Test
    void calculateInscribedCircleRadius() {
        assertEquals(2.4, rhombus.calculateInscribedCircleRadius());
    }

    @Test
    void getAllVertexCoordinates() {
        String expected = "(0,000; 3,000), (4,000; 6,000), (8,000; 3,000), (4,000; 0,000)";
        rhombus.showAllVertexCoordinates();

        assertEquals(expected, outputStreamCaptor.toString().trim());
    }

}